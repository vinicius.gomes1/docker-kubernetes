# KUBERNETES 



<br>

## Drupal con MYSQL por Kubernetes  
  
En este proyecto realizaremos el despliegue de una aplicación del tipo SaaS en Kubernetes. Para eso utilizaremos la aplicación DRUPAL, para la creación de sitios web, en conjunto con la Base de Datos MySql. 

El despliegue será del tipo Deployment, con 2 volúmenes persistentes: uno para el Drupal y otro para el MySql. 

El método incluye alta disponibilidad por medio de réplicas, a través de la estratégia RollingUgrade.

Y por último, se incluirá instrucciones para acceso interno via redireccionamiento de puertos.  


<br>

## Pre-requisitos
  
* Tener el microk8s instalado en una VM (utilizamos VirtualBox) con la versión reciente de Ubuntu. 

* Crear el un Alias para `microk8s kubectl`

* Bajar nuestro repositorio

<br>

### Instalación

Instalación del Snappy

```
$ sudo apt update && sudo apt install snapd
```

Se recomienda instalar los siguientes paquetes:
```
$ sudo apt install git vim net-tools openssh-server curl gnupg2
```

Instalar microk8s

```
$ sudo snap install microk8s --classic
```

Grupos de privilegio de administrador

```
$ sudo usermod -a -G microk8s $USER
$ sudo chown -f -R $USER ~/.kube
```

Status de Instalación y servicios activos

```
$ microk8s status --wait-ready
```

<br>

### ALIAS
En Kubernetes, el comando `microk8s` es utilizado para administrar el cluster; encuanto el `kubectl` se usa para administrar Kubernetes. 
Para facilicar nuestro trabajo con linea de comandos. Utilizaremos el alias `kubectl` para referirse a `microk8s kubectl`. 

Para ello, en el terminal de linux ejecutamos:
```
$ echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
```

Para aplicar los cambios (puede hacerse necesario un reinicio de sesión para efectivarlo):
```
$ source ~/.bashrc
```
<br>

### Bajar repositorio

Desde el terminal, tipear:
```
$ git clone https://gitlab.com/vinicius.gomes1/kubernetes.git
```

Realizar los procedimientos desde la carpeta bajada. 

<br>

## 1. Contraseña para usuario de base de Datos
Contaremos con un objeto exclusivo para gestión de la contraseña de usuario MySql, cuyo archivo es `kustomization.yaml`. 

Creación del objeto:  
```
$  kubectl apply -k . 
```

Listar el objeto creado para que sepamos su nombre:
```
$ kubectl get secrets

```

El nombre del objetos de llaves será importante en nuestro despliegue de base de datos posterior. En nuestro caso, su nombre es `mysql-pass-5m4tt9bbbt`. 

<br>

## 2. Preparando la infraestructura de datos persistentes
Como Kubernetes no tiene consistencia de datos, necesitamos crear volúmenes persistentes para preservalos. Caso contrario, todos los cambios realizados en la base de datos o cualquier otro directorio se perderia con el reinicio del contenedor. 

<br>

#### Creando PersistentVolumes (PV) para el MySql y Drupal

Empezamos con el PV para el MySql, por medio del fichero `drupal-mysql-pv.yaml`:

```
$ kubectl apply -f drupal-mysql-pv.yaml
```

Ahora creamos el PV para Drupal, con el fichero ` drupal-pv.yaml`:

```
$ kubectl apply -f drupal-pv.yaml
```

Para listar los PVs creados:
```
$  kubectl get pv
```

<br>

#### Creando PersistentVolumeClaim (PVC) para ambos MySql y Drupal

PVC de MySql:
```
$ kubectl apply -f drupal-mysql-pvc.yaml
```

PVC de Drupal:
```
$ kubectl apply -f drupal-pvc.yaml
```

Listar los PVCs creados:
```
$  kubectl get pvc
```

<br>

## 3. Hacer el Deploy de instancias MYSQL y Drupal

### Instancia MySql

El password del usuario MySql será aquél guardado por el objeto `mysql-pass-5m4tt9bbbt`, que generamos al principio de nuestro tutorial.

Antes de desplegarlo, deberás editar el manifiesto `mysql.yaml` e inserir el nombre del objeto de la clave, como especificado abajo:

```
  containers:
    - image: mysql:5.6
      name: drupal-mysql
      env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: **<INSERIR EL OBJETO-CÓDIGO>**

              key: password
```
```
$ kubectl apply -f mysql.yaml
```

### Instancia Drupal

Deployment de una instancia Drupal:

```
$ kubectl apply -f drupal.yaml
```

#### Comandos Importantes

##### Lista pods

```
$ kubectl get pods
```

##### Lista de deployment

```
kubectl get deployment
```

##### Información detallada

```
$ kubectl get pod drupal -o wide
```

`drupal-mysql`: nombre del pod
Nos tira la IP del pod, además de otras infos

##### Otra alternativa (más detallada):

```
$ kubectl describe drupal
```

##### Upgrade de la imagen de los contenedores:

```
$ kubectl set image Deployment mysql mysql=mysql:5.6 --all
```

`mysql`: nombre del objeto del tipo deployment



##### Reverter deployments (Rollback)

```
$ kubectl rollout undo deployment/drupal
```

##### Lista de pvc

```
$ kubectl get pvc
```

##### Lista de pv

```
$ kubectl get pv
```

##### Lista de servicios

```
$ kubectl get services
```

##### Lista de secrets

```
$ microk8s kubectl get secrets
```

##### Eliminar un pod: 

```
$ kubectl delete pod drupal
```

##### Para ejecutar comandos:

```
kubectl exec -it drupal --container drupal -- /bin/bash
```
El primer `drupal` se trata del objeto, y el segundo del contenedor. Dato importante si trabajamos con más de uno. Para nuestro caso vamos a tener sólo un contenedor

##### Buena aplicación para degub de yaml

```
yamllint
```


## 4. Redirección de puertos para acceder al servicio

```
$  kubectl port-forward --address 0.0.0.0 deployment/drupal 8080:80
```

En esta versión la redirección de puertos está hecha por Deployment. Para otras opciones, fijarse en el help:

``` 
$ kubectl port-forward --help
```
 











