# KUBERNETES / Docker

<br>

## App ABM de productos en Kubernetes con imagen en Docker 
  
Realizaremos un despliegue de una aplicación para ABM de productos, escrita en php, cuya base de datos erá almacenada en MariaBD. 

El despliegue será del tipo Deployment, con 2 volúmenes persistentes.  

El método incluye alta disponibilidad por medio de réplicas, a través de la estratégia RollingUgrade.

Y por último, se incluirá instrucciones para acceso interno via redireccionamiento de puertos.  

<br>

## Pre-requisitos
  
* Tener el microk8s instalado en una VM (utilizamos VirtualBox) con la versión reciente de Ubuntu. 

* Crear el un Alias para `microk8s kubectl`

* Bajar nuestro repositorio

<br>

### Instalación

Instalación del Snappy

```
$ sudo apt update && sudo apt install snapd
```
<br>

Se recomienda instalar los siguientes paquetes:
```
$ sudo apt install git vim net-tools openssh-server curl gnupg2
```
<br>

Instalar microk8s

```
$ sudo snap install microk8s --classic
```
<br>

Grupos de privilegio de administrador

```
$ sudo usermod -a -G microk8s $USER
$ sudo chown -f -R $USER ~/.kube
```
<br>

Status de Instalación y servicios activos

```
$ microk8s status --wait-ready
```

<br>

### ALIAS
En Kubernetes, el comando `microk8s` es utilizado para administrar el cluster; encuanto el `kubectl` se usa para administrar Kubernetes. 
Para facilicar nuestro trabajo con linea de comandos. Utilizaremos el alias `kubectl` para referirse a `microk8s kubectl`. 

Para ello, en el terminal de linux ejecutamos:
```
$ echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
```
<br>

Para aplicar los cambios (puede hacerse necesario un reinicio de sesión para efectivarlo):

```
$ source ~/.bashrc
```
<br>

### Bajar repositorio

Desde el terminal, tipear:
```
$ git clone https://gitlab.com/vinicius.gomes1/kubernetes.git
```
<br>

Realizar los procedimientos desde la carpeta bajada. 

<br>

## 1. Preparando la infraestructura de datos persistentes

Como Kubernetes no tiene consistencia de datos, necesitamos crear volúmenes persistentes para preservalos. Caso contrario, todos los cambios realizados en la base de datos o cualquier otro directorio se perderia con el reinicio del contenedor. 

<br>

#### Creando PersistentVolumes (PV) para el MySql y Apache

Empezamos con el PV para el MySql, por medio del fichero `mysql-pv.yaml`:

```
$ kubectl apply -f mysql-pv.yaml
```
<br>

Ahora creamos el PV para Apache, con el fichero ` apache-pv.yaml`:

```
$ kubectl apply -f apache-pv.yaml
```
<br>

Para listar los PVs creados:
```
$  kubectl get pv
```
<br>

#### Creando PersistentVolumeClaim (PVC) para ambos MySql y Apache

PVC de MySql:
```
$ kubectl apply -f mysql-pvc.yaml
```
<br>

PVC de Apache:
```
$ kubectl apply -f apache-pvc.yaml
```
<br>

Listar los PVCs creados:
```
$  kubectl get pvc
```
<br>

## 2. Hacer el Deploy de instancias MYSQL y Apache

### Instancia MySql

Deployment de una instancia MySql:

```
$ kubectl apply -f mysql.yaml
```
<br>

### Instancia Apache

Deployment de una instancia Apache:

```
$ kubectl apply -f apache.yaml
```
<br>

#### Comandos Importantes

##### Logs del pod

```
$ kubectl logs apache-cont
```
<br>

##### Lista pods

```
$ kubectl get pods
```
<br>

##### Lista de deployment

```
kubectl get deployment
```
<br>

##### Información detallada

```
$ kubectl get pod mysql -o wide
```

`mysql`: nombre del pod
Nos tira la IP del pod, además de otras infos
<br>

##### Otra alternativa (más detallada):

```
$ kubectl describe mysql
```
<br>

##### Upgrade de la imagen de los contenedores:

```
$ kubectl set image Deployment mysql mysql=mysql:5.6 --all
```

`mysql`: nombre del objeto del tipo deployment



##### Reverter deployments (Rollback)

```
$ kubectl rollout undo deployment/mysql
```

##### Lista de pvc

```
$ kubectl get pvc
```

##### Lista de pv

```
$ kubectl get pv
```

##### Lista de servicios

```
$ kubectl get services
```

##### Lista de secrets

```
$ microk8s kubectl get secrets
```

##### Eliminar un pod: 

```
$ kubectl delete pod mysql
```

##### Eliminar todos los pods y deployments

```
$ kubectl delete all --all --all-namespaces
```

##### Para ejecutar comandos:

```
kubectl exec -it mysql --container mysql -- /bin/bash
```
El primer `mysql` se trata del objeto, y el segundo del contenedor. Dato importante si trabajamos con más de uno. Para nuestro caso vamos a tener sólo un contenedor

##### Buena aplicación para degub de yaml

```
yamllint
```


## 3. Redirección de puertos para acceder al servicio

```
$  kubectl port-forward --address 0.0.0.0 deployment/apache 8080:80
```

En esta versión la redirección de puertos está hecha por Deployment. Para otras opciones, fijarse en el help:

``` 
$ kubectl port-forward --help
```
 











